import React from 'react';
import ReactDOM from 'react-dom';
import './index.scss';
import App from './App';
import * as serviceWorker from './serviceWorker';
import {Provider} from "react-redux";
import configureStore from "./components/store/store";
// import {SagaMiddleware as sagaMiddleware} from "@redux-saga/core/types";
import createSagaMiddleware from 'redux-saga'
import watchSome from "./components/store/saga/saga";



const reduxStore = configureStore({});


ReactDOM.render(
    <Provider store={reduxStore}>
        <App />
    </Provider>
    , document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
