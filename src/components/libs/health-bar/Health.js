import React from "react";
import './Health.scss';
import {useSelector} from "react-redux";

export const Health = (props) => {
    const units = useSelector(state => state.userReducer.user);
    const maxHp = props.maxHealth || 100;
    const unitHealth = ((props.health < 0 ? 0 : props.health) * 100) / maxHp;
    return (
        <div className={'health-component'}>
            <span
                style={{width: `${unitHealth}%`}}
                className={`health-bar ${`${units}` !== props.unitId ? 'red' : ''}`}>

            </span>
        </div>
    )
};
