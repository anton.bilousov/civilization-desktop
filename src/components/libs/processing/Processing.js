import React from "react";
import './Processing.scss'
import {useDispatch} from "react-redux";
import {createOne} from "../../store/actions/city.action";
import {image} from "../images/images";
import {Health} from "../health-bar/Health";

const styles = {
    width: '128px'
};
export const Processing = (props) => {
    const dispatch = useDispatch();
    return (
        <div className={'processing'}>
            <span style={{position: "relative"}}><img style={styles} src={image(props.unit.img)} alt={'no'} />
                <span>
                    <Health health={props.unit.hp} maxHealth={props.unit.maxHealth}/>
                </span>
            </span>
            <div>Move: {props.unit.move}
            </div>
            <div>Strength: {props.unit.strength}</div>
            <button onClick={() => dispatch(createOne(props.unit))}>create city</button>
        </div>
    )
};
