import Scout from "../../../img/Scout.png";
import Settler from "../../../img/Settler.png";

export const image = (img) => {
    switch (img) {
        case './assets/img/Scout.png': {
            return Scout;
        }
        case './assets/img/Settler.png': {
            return Settler
        }
        default: {
            return Scout
        }
    }
};
