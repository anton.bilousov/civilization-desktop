import React from "react";
import {unitMakeMove} from "../../store/actions/unit.action";
import {useDispatch} from "react-redux";

export const Field = (props) => {
    const dispatch = useDispatch();
    return (
        <div className={'hex'} onClick={() => dispatch(unitMakeMove(props.coordinate))}>
            <div className="middle">{props.coordinate.y}</div>
        </div>
    )
};
