import * as io from "socket.io-client";

const connectionOptions =  {
    'force new connection' : true,
    reconnectionAttempts: 'Infinity',
    timeout : 10000,
    transports : ['websocket'],
    extraHeaders: {
        Authorization: '1112'
    }
};
class CreateConnection {
    constructor() {
    }
    set connect(conection) {
        this._socket = conection
    }
    get connect() {
        return this._socket
    }
    start() {
        this.connect = io('http://localhost:4500', connectionOptions);
        return this;
    }
}
export const SocketIo = new CreateConnection();
