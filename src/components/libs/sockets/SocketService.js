import React, {Component} from "react";
// import * as io from 'socket.io-client';
import {connect} from "react-redux";
import { createItem } from '../../store/actions/user.actions';
// import userReducer from "../../store/reducers/user.reducers";
import {SocketContext} from "./SocketContext";
import {createUnit, unitMove} from "../../store/actions/unit.action";
import {addLobby} from "../../store/actions/lobby.action";
import {createMatrix} from "../../store/actions/matrix.action";
import {createCity} from "../../store/actions/city.action";
import {addEnemy, enemyMove} from "../../store/actions/enemy.actions";
import {SocketIo} from "./SocketSave";



class Socket extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            test: 'ddd'
        };
        // this.socket = io('http://localhost:4500', connectionOptions);
        // saveContext(this.socket);
        this.socket = SocketIo.start().connect;

        this.socket.status = 'initialized';
        this.socket.on('connect', () => {
            this.socket.status = 'connected';
            console.log('connected');
        });

        this.socket.on('disconnect', () => {
            this.socket.status = 'disconnected';
            console.log('disconnected');
        });
        this.socket.on('getMatrix', (matrix) => {
            this.props.addMatrix(matrix);
        });
        this.socket.on('unitMove', (unit) => {
            this.props.unitMove(unit[unit.length - 1]);
        });
        this.socket.on('unit', (units) => {
            this.props.addUnit(units);
        });
        this.socket.on('city', (city) => {
            this.props.addCity(city);
        });
        this.socket.on('enemy', (enemy) => {
            this.props.addEnemy(enemy);
        });
        this.socket.on('enemyMove', (moves) => {
            this.props.enemyMove(moves);
        });
    }

    render() {
        return (
            <SocketContext.Provider value={this.socket}>
                {this.props.children}
            </SocketContext.Provider>
            );
    }
}


const stateCurrent = (state) => ({state});
const dispatchCurrent = (dispatch) => {
    return {
        add: (menu) => {
            dispatch(createItem(menu));
        },
        addUnit: (units) => {
            dispatch(createUnit(units));
        },
        unitMove: (unit) => dispatch(unitMove(unit)),
        addLobby: (lobby) => {
            dispatch(addLobby(lobby));
        },
        addMatrix: (matrix) => dispatch(createMatrix(matrix)),
        addEnemy: (enemy) => dispatch(addEnemy(enemy)),
        enemyMove: (move) => dispatch(enemyMove(move)),
        addCity: (cities) => dispatch(createCity(cities))
    }
};
export default connect(stateCurrent, dispatchCurrent)(Socket)
