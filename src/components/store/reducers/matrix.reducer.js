import {matrixTypes} from "../actions/matrix.action";

const defaultState = {matrix: [[]]};

const matrixReducer = (state = defaultState, action) => {
    switch (action.type) {
        case matrixTypes.ADD_MATRIX: {
            return {...state, matrix: action.payload};
        }
        default: {
            return state;
        }
    }
};

export default matrixReducer;
