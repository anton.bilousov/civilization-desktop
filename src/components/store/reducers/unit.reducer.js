import {unitTypes} from "../actions/unit.action";

const defaultState = {units: []};

const unitReducer = (state = defaultState, action) => {
    switch (action.type) {
        case unitTypes.ADD_UNIT: {
            return {...state, units: action.payload};
        }
        case unitTypes.UNIT_MOVE: {
            const test = state.units.map(el => {
                if (el.id === action.payload.id) {
                    return {...el, ...action.payload};
                } else {
                    return el;
                }
            });
            return {...state, units: test};
        }
        default: {
            return state;
        }
    }
};

export default unitReducer;
