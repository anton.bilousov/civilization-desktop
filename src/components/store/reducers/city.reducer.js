import {cityTypes} from "../actions/city.action";

const defaultCities = {city: []};

export const cityReducer = (state = defaultCities, action) => {
    switch (action.type) {
        case cityTypes.ADD_CITY: {
            return {...state, city: action.payload}
        }
        default: {
            return state;
        }
    }
};
