import {enemyTypes} from "../actions/enemy.actions";

const initialState = {enemy: {}};

export const enemyReducer = (state = initialState, action) => {
    switch (action.type) {
        case enemyTypes.ADD_ENEMY: {
            state.enemy[action.payload.userId] = {
                units: action.payload.units,
                cities: action.payload.cities
            };
            return {...state};
        }
        case enemyTypes.ENEMY_MOVE: {
            // TODO: delete when unit animation will be add
            const payload = action.payload;
            const newUnit = payload.unitsMove[payload.unitsMove.length - 1];
            const units = state.enemy[payload.userId].units.map(unit => (
                unit.id === newUnit.id ? newUnit : unit
            ));

            state.enemy[payload.userId].units = units;
            return {...state};
        }
        default: {
            return state;
        }
    }
};
