import {combineReducers} from "redux";
import userReducer from "./user.reducers";
import unitReducer from "./unit.reducer";
import {lobbyReducer} from "./lobby.reducer";
import matrixReducer from "./matrix.reducer";
import {cityReducer} from "./city.reducer";
import {enemyReducer} from "./enemy.reducers";
import {processingReducer} from "./processing.reducers";


const rootReducers = combineReducers({
    userReducer,
    unitReducer,
    lobbyReducer,
    matrixReducer,
    cityReducer,
    enemyReducer,
    processingReducer
});
export default rootReducers
