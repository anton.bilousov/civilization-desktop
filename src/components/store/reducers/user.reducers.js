
import ACTIONS from "../actions/user.actions";

const defaultState = {};

const userReducer = (state = defaultState, action) => {
    switch (action.type) {
        case ACTIONS.Types.CREATE_USER: {
            return {...state, user: action.payload};
        }

        default:
            return state;
    }
};

export default userReducer;
