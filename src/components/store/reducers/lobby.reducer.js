import {lobbyTypes} from "../actions/lobby.action";
const defaultState = {lobby: {users: []}};

export const lobbyReducer = (state = defaultState, action) => {
    switch (action.type) {
        case lobbyTypes.ADD_LOBBY: {
            return {...state, lobby: action.payload};
        }
        default: {
            return state;
        }
    }
}
