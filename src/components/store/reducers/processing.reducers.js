import {enemyTypes} from "../actions/enemy.actions";
import {unitTypes} from "../actions/unit.action";

const initialState = {processing: null};

export const processingReducer = (state = initialState, action) => {
    switch (action.type) {
        case unitTypes.PROCESSING: {
            return {...state, processing: action.payload};
        }
        default: {
            return state;
        }
    }
};
