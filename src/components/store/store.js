import { createStore, applyMiddleware } from "redux";

import rootReducers from "./reducers";
import createSagaMiddleware from "redux-saga";
import watchSome from "./saga/saga";

export default function configureStore(initialState) {
    const sagaMiddleware = createSagaMiddleware();
    const store = createStore(rootReducers, initialState, applyMiddleware(sagaMiddleware));
    window.store = store;
    sagaMiddleware.run(watchSome);
    return store;
}
