import {call, put, select} from "@redux-saga/core/effects";
import {SocketIo} from "../../libs/sockets/SocketSave";

const getState = (state) => state.processingReducer.processing;

export default function* unitMove({ payload }) {
    const state = yield select(getState);
    const socket = SocketIo.connect;
    if (state) {
        yield call(() => socket.emit('unitCanMove', {from: state, to: payload}));
        yield put({type: 'PROCESSING', payload: null});
    }
}
