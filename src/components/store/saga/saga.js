import { takeEvery } from "@redux-saga/core/effects";
import unitMove from "./unit-move.saga";
import createCity from "./create-city.saga";

export default function* watchSome() {
    yield takeEvery('MAKE_MOVE',unitMove);
    yield takeEvery('CREATE_ONE', createCity);
}



