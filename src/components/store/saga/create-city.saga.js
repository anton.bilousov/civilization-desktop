import {SocketIo} from "../../libs/sockets/SocketSave";
import {call, put} from "@redux-saga/core/effects";

export default function* createCity({payload}) {
    const socket = yield SocketIo.connect;
    yield call(() => socket.emit('createCity', payload));
    yield put({type: 'PROCESSING', payload: null});
}
