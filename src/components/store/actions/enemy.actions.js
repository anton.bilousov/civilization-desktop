
export const enemyTypes = {
    ADD_ENEMY: 'ADD_ENEMY',
    ENEMY_MOVE: 'ENEMY_MOVE'
};

export const addEnemy = (enemy) => ({
    type: enemyTypes.ADD_ENEMY,
    payload: enemy
});

export const enemyMove = (moves) => ({
    type: enemyTypes.ENEMY_MOVE,
    payload: moves
});
