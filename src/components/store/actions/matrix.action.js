export const matrixTypes = {
    ADD_MATRIX: 'ADD_MATRIX',
    FIND_UNIT: 'FIND_UNIT',
    UNIT_MOVE: 'UNIT_MOVE'
};

export const createMatrix = matrix => ({
    type: matrixTypes.ADD_MATRIX,
    payload: matrix
});

export const unitMove = matrix => ({
    type: matrixTypes.ADD_MATRIX,
    payload: matrix
});
