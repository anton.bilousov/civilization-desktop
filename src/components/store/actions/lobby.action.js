export const lobbyTypes = {
    ADD_LOBBY: 'ADD_LOBBY'
};

export const addLobby = (payload) => ({
    type: lobbyTypes.ADD_LOBBY,
    payload
});
