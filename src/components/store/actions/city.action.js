export const cityTypes = {
    ADD_CITY: 'ADD_CITY',
    CREATE_ONE: 'CREATE_ONE',
    UNIT_MOVE: 'UNIT_MOVE'
};

export const createCity = city => ({
    type: cityTypes.ADD_CITY,
    payload: city
});

export const createOne = city => ({
    type: cityTypes.CREATE_ONE,
    payload: city
});
