export const unitTypes = {
    ADD_UNIT: 'ADD_UNIT',
    FIND_UNIT: 'FIND_UNIT',
    UNIT_MOVE: 'UNIT_MOVE',
    PROCESSING: 'PROCESSING',
    MAKE_MOVE: 'MAKE_MOVE'
};

export const createUnit = task => ({
    type: unitTypes.ADD_UNIT,
    payload: task
});

export const unitMove = task => ({
    type: unitTypes.UNIT_MOVE,
    payload: task
});

export const addProcessing = payload => ({
    type: unitTypes.PROCESSING,
    payload
});
export const unitMakeMove = payload => ({
    type: unitTypes.MAKE_MOVE,
    payload
});
