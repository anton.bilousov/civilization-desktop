import React from "react";
import {SocketContext} from "../../libs/sockets/SocketContext";
import {InputField} from "../../libs/inputs/Input-field";

class Lobby extends React.Component {

    constructor(props) {
        super(props);
        this.addLobby = this.addLobby.bind(this);
        this.startGame = this.startGame.bind(this);
        this.state = {
            users: [],
            name: ''
        };
        console.log(this.props.lobby);

    }

    componentDidMount() {
        const socket = this.context;
        socket.on('lobby', (lobbyData) => {
            this.setState({users: lobbyData.users})
        });
        socket.on('start', () => {
            socket.emit('user', {user: '319'});
            this.props.history.push('/game');
        })
    }

    startGame() {
        const socket = this.context;
        socket.emit('start', this.state.name);
    }

    addLobby() {
        // TODO: add unit id
        console.log(this.state.name);
        const socket = this.context;
        socket.emit('lobby', {
            key: '319',
            name: this.state.name
        })
    }

    render() {
        return (
            <>
                <label>Room name*:</label>
                <InputField onChang={(e) => this.setState({name: e})} />
                <button onClick={this.addLobby}>Go</button>
                {this.state.users.map(user => {
                    return (
                        <div>
                            <span>{user.userName}</span>
                            <span>{user.leader}</span>
                        </div>
                    )
                })}
                {this.state.users.length > 0 ? <button onClick={this.startGame}>go</button> : ''}
            </>
        )
    }
}

Lobby.contextType = SocketContext;


export default Lobby;
