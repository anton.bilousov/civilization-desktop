import React from "react"
import {InputField} from "../../libs/inputs/Input-field";
import {SocketContext} from "../../libs/sockets/SocketContext";
import {createItem} from "../../store/actions/user.actions";
import {connect} from "react-redux";
import logo from '../../../img/logo.svg';

class LoginPage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {login: 'Login', password: 'Password'};
        this.handler = this.handler.bind(this);

    }
    componentDidMount() {
        this.electron = window.require('electron');
        this.electron.ipcRenderer.send('menu:add');
        this.electron.ipcRenderer.on('menu:add', (e, item) => {
            this.props.add(item.user);
        });
    }

    handler(e) {
        this.electron.ipcRenderer.send('menu:add', {...this.state});
    }

    redirectYoLobby() {
        if (this.props.user) {
            this.props.history.push('/firstPage');
        }

    }

    render() {
        return (
            <>
                {this.redirectYoLobby()}
                <img src={logo}/>
                <InputField onChang={(e) => this.setState({login: e})} type="text"/>
                <InputField onChang={(e) => this.setState({password: e})} type="password"/>
                <button onClick={this.handler}>Test</button>
            </>
        )
    }
}

LoginPage.contextType = SocketContext;

const stateCurrent = (state) => ({user: state.userReducer.user || ''});
const dispatchCurrent = (dispatch) => {
    return {
        add: (menu) => {
            dispatch(createItem(menu))
        }
    }
};
export default connect(stateCurrent, dispatchCurrent)(LoginPage)
