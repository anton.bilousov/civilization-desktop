import React from "react";
import {Matrix} from "./modules/matrix/Matrix";
import { useSelector} from "react-redux";
import {Processing} from "../../libs/processing/Processing";
import {Cities} from "./modules/city/Cities";
import {UnitBuilder} from "./modules/units/UnitBuilder";
import {EnemyBuilder} from "./modules/units/EnemyBuilder";

export const Game = (props) => {
    const state = useSelector(state => state);
    return (
        <>
            <Matrix landscape={state.matrixReducer.matrix} />
            <UnitBuilder />
            {state.processingReducer.processing ?
                <Processing unit={state.processingReducer.processing}/> :
                null}
            {
                state.cityReducer.city.map((el, i) => (
                    <Cities key={i} city={el}/>
                ))
            }
            <EnemyBuilder enemy={state.enemyReducer.enemy}/>
            {/*{*/}
            {/*    this.props.state.enemyReducer.enemy*/}
            {/*}*/}
        </>
    )
};
