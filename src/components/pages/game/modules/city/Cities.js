import React from "react";
import City from '../../../../../img/civ.png';
import './Cities.scss';
import { useHistory } from 'react-router-dom'

export const Cities = (props) => {
    const history = useHistory();
    return (
        <div
            className='units-container'
            style={{
                top: `${props.city.x * 100}px`,
                left: `${props.city.y * 100}px`}
            }>
            <img src={City} onClick={() => history.push(`/city/${props.city.id}`)}/>
        </div>
    )
};
