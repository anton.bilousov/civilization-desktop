import React from "react";
import './Game.scss'
import {Field} from "../../../../libs/field/Field";

export const Matrix = (props) => {
    return (<div>
        {props.landscape.map((el, indexOnX) => {
            return (
                <div key={indexOnX} className={`hex-row`}>{el.map((ele, indexOnY) => (
                        <Field key={indexOnY} coordinate={{x:indexOnX, y: indexOnY}}/>
                    )
                )}</div>
            )
        })}
    </div>)
};
