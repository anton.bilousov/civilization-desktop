import React from "react";
import './Units.scss'
import {image} from "../../../../libs/images/images";
import {Health} from "../../../../libs/health-bar/Health";
import {useDispatch} from "react-redux";
import {addProcessing} from "../../../../store/actions/unit.action";

export const Unit = (props) => {
    const dispatch = useDispatch();
    const styles = {
        position: 'absolute'
    };
    return (
        <div
            className='units-container'
            style={{...styles,
                top: `${props.unit.x * 100}px`,
                left: `${props.unit.y * 100}px`}
            }>
            <img src={image(props.unit.img)} onClick={() => dispatch(addProcessing(props.unit))}/>
            <Health
                health={props.unit.hp}
                maxHealth={props.unit.maxHealth}
                unitId={props.unit.userId}/>
        </div>
    )
};
