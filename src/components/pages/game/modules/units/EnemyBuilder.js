import React from "react";
import { Unit } from "./Units";
import {Cities} from "../city/Cities";

export const EnemyBuilder = (props) => {
    let units = [], cities = [];
    for (let key in props.enemy) {
        const data = props.enemy[key];
        if (data.units) units.push(...data.units);
        if (data.cities) cities.push(...data.cities);
    }
    return (
        <>
            {
                units.map(el => <Unit unit={el} />)
            }
            {
                cities.map(el => <Cities city={el} />)
            }
        </>
    )
};
