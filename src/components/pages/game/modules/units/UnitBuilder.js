import React from "react";
import {useSelector} from "react-redux";
import {Unit} from "./Units";

export const UnitBuilder = props => {
    const units = useSelector(state => state.unitReducer.units);
    return (
        <>{
            units.map(el => {
                return <Unit unit={el} />
            })
        }</>
    )
};
