import React from "react";
import {
    useParams,
    useHistory
} from "react-router-dom";
import {useSelector} from "react-redux";
import City from '../../../../../img/civ.png';

export const CityPage = (props) => {
    const {id} = useParams();
    const city = useSelector(state => {
        return state.cityReducer.city.find(element => (
            element.id === id
        ))
    });
    const history = useHistory();
    return (
        <div>
            <h1 onClick={() => history.goBack()}>{'< Back'}</h1>
            <img src={City}/>
            <span>Id: {city.id}</span>
        </div>
    )
};
