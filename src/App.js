import React from 'react';

import './App.css';
import {
    Route,
    Link, HashRouter
} from "react-router-dom";
import Socket from "./components/libs/sockets/SocketService";
import LoginPage from './components/pages/login-page/Login'
import { Game } from "./components/pages/game/Game.page";
import Lobby from "./components/pages/lobby/Lobby";
import {CityPage} from "./components/pages/game/modules/city-page/CityPage";

let appProviderRef;
export const getProviderState = () => appProviderRef;
function App() {
  return (
      <HashRouter>
          <Socket>
              <Route path="/" exact component={ LoginPage } />
              <Route path="/firstPage"  component={ Lobby } />
              <Route path="/game"  component={ Game } />
              <Route path="/city/:id"  component={ CityPage } />
          </Socket>
          <Link to={''} >HHHHH</Link>
          <Link to={'/firstPage'}>SSSSSS</Link>
          <Link to={'/city/3'}>LLLLLLL</Link>
      </HashRouter>
  );
}

export default App;
