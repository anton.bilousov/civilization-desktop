const axios = require('axios');

class ApiService {
    sendLogin(login) {
        return axios.post('https://civilization-server-dev.herokuapp.com/user', login);
    }
}

module.exports = ApiService;
