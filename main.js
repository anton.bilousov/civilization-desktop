const { app, BrowserWindow, ipcMain } = require('electron');
const test = require('./services/common');
const api = require('./services/api.service');
const fs = require('fs');

const ApiService = new api();
const savePath = `${app.getPath('documents')}/save.json`;

let win;

ipcMain.on('menu:add', (e, data) => {
  if (data) {
    ApiService.sendLogin(data).then((result) => {
      const data = JSON.stringify({user: result.data});
      fs.writeFile(savePath, data, () => {
        win.webContents.send('menu:add', result.data);
      });
    });
  } else {
    fs.readFile(savePath, (err, data) => {
      if (err) {
        console.log(err);
      } else {
        const obj = JSON.parse(data);
        win.webContents.send('menu:add', obj);
      }
    })
  }
});

function createWindow () {
  win = new BrowserWindow({
    width: test() ? 1200 : 100,
    height: 600,
    webPreferences: {
      nodeIntegration: true
    }
  });

  win.loadURL(`file://${__dirname}/build/index.html`)

  win.webContents.openDevTools()
}

app.whenReady().then(createWindow);

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
});

app.on('activate', () => {
  if (BrowserWindow.getAllWindows().length === 0) {
    createWindow()
  }
});

